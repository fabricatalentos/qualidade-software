from src.operations.sub import SubOperations

from faker import Faker

fake = Faker()

def test_sub():

    subOperations = SubOperations()

    num1 = fake.random_number()
    num2 = fake.random_number()

    expected_sub = num1 - num2

    result = subOperations.sub(num1,num2)

    assert result == expected_sub