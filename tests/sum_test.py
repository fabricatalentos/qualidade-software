from src.operations.sum import SumOperations

from faker import Faker

fake = Faker()

def test_sum():

    sumOperations = SumOperations()

    num1 = fake.random_number()
    num2 = fake.random_number()

    expected_sum = num1 + num2

    result = sumOperations.sum(num1,num2)

    assert result == expected_sum